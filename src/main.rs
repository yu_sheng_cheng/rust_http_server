use actix_web::{get, middleware, web, App, HttpRequest, HttpResponse, HttpServer, post, HttpMessage, error, Error, Responder};
use json::JsonValue;
use serde::{Deserialize, Serialize};
use std::borrow::Borrow;
use actix_web::web::Form;
use serde_json::ser::State;

#[macro_use]
extern crate log;

#[derive(Debug, Serialize, Deserialize)]
struct MyObj {
    name: String,
    number: i32,
}

#[get("/resource1/{name}/index.html")]
async fn index(req: HttpRequest, name: web::Path<String>) -> String {
    println!("REQ: {:?}", req);
    format!("Hello: {}!\r\n", name)
}

const MAX_SIZE: usize = 262_144; // max payload size is 256k

const API_VERSION: &str  = "v1";


async fn index_async(req: HttpRequest) -> &'static str {
    println!("REQ!: {:?}", req);
    "Hello world!\r\n"
}

#[get("/")]
async fn no_params() -> &'static str {
    "Hello world!\r\n"
}

async fn kds(data: String) ->  &'static str {
    // Body of the function goes here!
    info!("received {}", data);
    "{\"result\":\"ok\"}"

}


#[actix_web::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "info");
    std::env::set_var("actix_server","info");
    std::env::set_var("actix_web","info");
    env_logger::init();
    HttpServer::new(|| {
        App::new()
            .wrap(middleware::DefaultHeaders::new().header("X-Version", API_VERSION))
            .wrap(middleware::Compress::default())
            .wrap(middleware::Logger::default())
            .service(index)
            .service(no_params)
            .service(
                web::scope(&*format!("/api/{}/", API_VERSION))
                    .route("/kds", web::post().to(kds)))
            .service(
                web::resource("/resource2/index.html")
                    .wrap(
                        middleware::DefaultHeaders::new().header("X-Version-R2", "0.3"),
                    )
                    .default_service(
                        web::route().to(|| HttpResponse::MethodNotAllowed()),
                    )
                    .route(web::get().to(index_async)),
            )
            .service(web::resource("/test1.html").to(|| async { "Test\r\n" }))

    })
        .bind("0.0.0.0:8080")?
        .workers(1)
        .run()
        .await
}