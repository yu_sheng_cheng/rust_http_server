# test server from command line

## install on  MacOs
```


brew install httpie

```

## try httpie
```
 http post 127.0.0.1:8080/api/v1/kds body='{name="cc"}'
```

## fast compile check
```
cargo check
```

## testbed
```
scp  -P 8787  auto_order_entry eric@122.117.90.137:/home/eric/ 

```